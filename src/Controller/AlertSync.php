<?php

namespace Drupal\rave_alerts\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rave_alerts\Plugin\ApiTools\RaveClient;
use Drupal\rave_alerts\Rave\SyncManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AlertSync extends ControllerBase {

  /**
   * @var \Drupal\rave_alerts\Plugin\ApiTools\RaveClient
   */
  protected $client;

  /**
   * @var \Drupal\rave_alerts\Rave\SyncManager
   */
  protected $syncManager;

  /**
   * NotificationController constructor.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param RaveClient $rave_client
   *   The rave apitools api client plugin.
   * @param SyncManager $sync_manager
   *   The rave sync manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RaveClient $rave_client, SyncManager $sync_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->client = $rave_client;
    $this->syncManager = $sync_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.apitools_client')->load('rave'),
      $container->get('rave_alerts.sync_manager')
    );
  }

  /**
   * Builds the response.
   */
  public function sync(Request $request) {
    $responses = $this->client->sync();
    $count = 0;
    foreach ($responses as $mapping_id => $response) {
      foreach ($response as $result_row) {
        $this->syncManager->syncAlert($mapping_id, $result_row, $count);
      }
    }
    $code = $count > 0 ? Response::HTTP_CREATED : Response::HTTP_OK;
    return new Response($count, $code);
  }
}