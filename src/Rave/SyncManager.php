<?php

namespace Drupal\rave_alerts\Rave;

use Drupal\apitools\SyncManager as SyncManagerBase;
use Drupal\apitools\ClientManagerInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldDefinition;
use Drupal\Core\Queue\QueueFactory;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use http\Exception\InvalidArgumentException;

/**
 * Defines an Rave implementation for SyncManager Class.
 */
class SyncManager extends SyncManagerBase {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * SyncManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\apitools\ClientManagerInterface $client_manager
   *   ApiTools client plugin manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   Queue factory.
   */
  public function __construct(EntityTypeManager $entity_type_manager, ClientManagerInterface $client_manager, QueueFactory $queue_factory, ConfigFactoryInterface $config_factory) {
    parent::__construct($entity_type_manager, $client_manager, $queue_factory);
    $this->config = $config_factory->get('rave_alerts.settings');
  }

  private static function config() {
    return \Drupal::configFactory()->get('rave_alerts.settings');
  }

  /**
   * {@inheritdoc}
   */
  protected function getClientPluginId() {
    return 'rave';
  }

  /**
   * Returns array of Rave Alerts specific base field definitions.
   *
   * @see \rave_alerts_entity_base_field_info()
   */
  public static function entityBaseFieldInfo(EntityTypeInterface $entity_type) {
    $fields = [];

    if (empty(static::getEnabledEntityTypeBundles($entity_type->id()))) {
      return $fields;
    }

    $fields['rave_identifier'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Rave identifier'))
      ->setDescription(t('Remote ID for the GetRave API.'))
      ->setSetting('max_length', 128);

    return $fields;
  }

  /**
   * Returns array of Rave Alerts specific bundle field definitions.
   *
   * @see \rave_alerts_entity_bundle_field_info()
   */
  public static function entityBundleFieldInfo(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = [];

    $bundles = static::getEnabledEntityTypeBundles($entity_type->id());
    if (empty($bundles[$bundle])) {
      return $fields;
    }

    $fields['rave_identifier'] = FieldDefinition::createFromFieldStorageDefinition($base_field_definitions['rave_identifier'])
      ->setLabel(t('Rave identifier'))
      ->setDescription(t('Remote ID for the GetRave API.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  private static function getEnabledEntityTypeBundles($entity_type_id) {
    $mapping_definitions = static::config()->get('mapping');
    $bundles = [];
    foreach ($mapping_definitions as $mapping) {
      if (empty($mapping['sync_' . $entity_type_id])) {
        continue;
      }
      $bundle = $mapping['sync_' . $entity_type_id . '_bundle'];
      $bundles[$bundle] = $bundle;
    }
    return $bundles;
  }

  /**
   * Sync result row from API to entity.
   *
   * @param array $result
   *   The result row from the API.
   * @param int $counter
   *   Incremented if entity does not exist and is created.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function syncAlert($id, array $result, &$counter = 0) {
    $mapping_definitions = $this->config->get('mapping');
    if (empty($mapping_definitions[$id])) {
      throw new \InvalidArgumentException('Invalid mapping id ' . $id);
    }
    $mapping = $mapping_definitions[$id];
    // If notification_message is not enabled the key will not exist at all.
    $sync_notification = !empty($mapping['sync_notification_message']) && $this->syncNotification($result, $mapping);
    $sync_node = $mapping['sync_node'] && $this->syncNode($result, $mapping);

    if ($sync_node || $sync_notification) {
      $counter++;
    }
  }

  public function syncNode($result, $mapping) {
    $identifier = $result['identifier'];

    $nodes = $this->entityTypeManager->getStorage('node')->loadByProperties([
      'type' => $mapping['sync_node_bundle'],
      'rave_identifier' => $identifier
    ]);
    if (empty($nodes)) {
      $values = [
        'type' => $mapping['sync_node_bundle'],
        'rave_identifier' => $identifier,
      ];

      $mapping = $mapping['sync_node_mapping'] ?: [];
      foreach ($mapping as $drupal_field_key => $rave_api_key) {
        $drupal_value = $this->getNestedResultValue($result, $rave_api_key);
        $this->setNestedDrupalValue($values, $drupal_field_key, $drupal_value);
      }

      $node = $this->entityTypeManager->getStorage('node')->create($values);
      $node->save();
      return TRUE;
    }
    return FALSE;
  }

  private function getNestedResultValue(array $result, $parents) {
    $this->processStringSeparators($parents);
    $parents = !is_array($parents) ? [$parents] : $parents;
    return NestedArray::getValue($result, $parents);
  }

  private function setNestedDrupalValue(&$values, $drupal_field_key, $drupal_value) {
    $this->processStringSeparators($drupal_field_key);
    NestedArray::setValue($values, $drupal_field_key, $drupal_value, TRUE);
  }

  private function processStringSeparators(&$string) {
    if (!is_array($string)) {
      $string = explode('/', $string);
    }
  }

  public function syncNotification($result, $mapping) {
    $identifier = $result['identifier'];
    $messages = $this->entityTypeManager->getStorage('notification_message')->loadByProperties([
      'type' => 'global',
      'rave_identifier' => $identifier
    ]);
    if (empty($messages)) {
      $publish_start_date = new DrupalDateTime($result['sent']);
      $publish_end_date = clone $publish_start_date;
      $publish_end_date->add(new \DateInterval('P1W'));
      $message = $this->entityTypeManager->getStorage('notification_message')->create([
        'type' => 'global',
        'label' => $result['info']['headline'] ?? '',
        'message' => $result['info']['description'] ?? '',
        'rave_identifier' => $identifier,
        'publish_start_date' => $publish_start_date->format(DateTimeItem::DATETIME_STORAGE_FORMAT),
        'publish_end_date' => $publish_end_date->format(DateTimeItem::DATETIME_STORAGE_FORMAT),
      ]);
      $message->save();
      return TRUE;
    }
    return FALSE;
  }
}
