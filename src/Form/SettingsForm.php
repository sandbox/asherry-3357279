<?php

namespace Drupal\rave_alerts\Form;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Dumper;

class SettingsForm extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected $entityDefinitionUpdateManager;

  /**
   * Constructs a rave alerts SettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler, EntityFieldManagerInterface $entity_field_manager, EntityDefinitionUpdateManagerInterface $entity_definition_update_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityDefinitionUpdateManager = $entity_definition_update_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('entity_field.manager'),
      $container->get('entity.definition_update_manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rave_alerts.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rave_alerts_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['mapping'] = ['#type' => 'container', '#tree' => TRUE];
    $mapping_definitions = $this->config('rave_alerts.settings')->get('mapping') ?? [];
    $c = $this->config('rave_alerts.settings');
    foreach ($mapping_definitions as $mapping) {
      $id = $mapping['id'];
      $form['mapping'][$id] = $this->buildElement($mapping);
    }

    $form['mapping']['_new'] = $this->buildElement([], FALSE);

    $form['field_statuses'] = [
      '#title' => $this->t('Field statuses'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $form['field_statuses']['node'] = [
      '#type' => 'item',
      '#title' => $this->t('Node'),
      '#markup' => $this->isFieldInstalled('node') ? $this->t('Installed') : $this->t('Not installed'),
    ];

    if ($this->moduleHandler->moduleExists('notification_message')) {
      $form['field_statuses']['notification_message'] = [
        '#type' => 'item',
        '#title' => $this->t('Notification Message'),
        '#markup' => $this->isFieldInstalled('notification_message') ? $this->t('Installed') : $this->t('Not installed'),
      ];
    }

    return $form;
  }

  public function machineNameExists($value, array $element, FormStateInterface $form_state) {
    // This should be improved to allow the user to re-order.
    $mapping = $form_state->getValue('mapping');
    return array_key_exists($value, $mapping);
  }

  protected function buildElement(array $config, $open = TRUE) {
    $id = $config['id'] ?? '_new';

    $element = [
      '#type' => 'details',
      '#title' => $config['label'] ?? 'New',
      '#open' => $open,
    ];

    $element['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $config['label'] ?? '',
    ];

    $element['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('ID'),
      '#default_value' => $config['id'] ?? '',
      '#machine_name' => ['exists' => [$this, 'machineNameExists']],
      '#required' => FALSE,
    ];

    $element['outbound_url'] = [
      '#title' => $this->t('Outbound URL'),
      '#type' => 'textfield',
      '#default_value' => $config['outbound_url'] ?? '',
    ];

    if ($this->moduleHandler->moduleExists('notification_message')) {
      $element['sync_notification_message'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Sync to notification'),
        '#description' => $this->t('Sync alerts to Notification Message entities to display in a block'),
        '#default_value' => $config['sync_notification_message'] ?? 0,
      ];

      $element['sync_notification_message_bundle'] = $this->getBundleSelectElement(
        $id,
        'notification_message',
        'notification_message_type',
        'Notification Message',
        $config['sync_notification_message_bundle'] ?? NULL,
      );
    }

    $element['sync_node'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sync to content'),
      '#description' => $this->t('Sync alerts to a specified node bundle'),
      '#default_value' => $config['sync_node'] ?? NULL,
    ];

    $element['sync_node_bundle'] = $this->getBundleSelectElement(
      $id,
      'node',
      'node_type',
      'Node',
      $config['sync_node_bundle'] ?? NULL
    );

    $dumper = new Dumper(2);
    $sync_node_mapping = $config['sync_node_mapping'] ?? NULL;
    $element['sync_node_mapping'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mapping'),
      '#default_value' => $sync_node_mapping ? $dumper->dump($sync_node_mapping, 4) : '',
      '#states' => [
        'visible' => $this->getCheckedInputStatesValue($id, 'node'),
      ],
      '#description' => $this->t('Ensure that all indentation is consistent (2 spaces) and there are no trailing characters.
      Top level keys are Drupal field names and values are array representations of the api response passed to NestedArray::getValue.')
    ];

    return $element;
  }

  private function isFieldInstalled($entity_type_id) {
    return !empty($this->entityDefinitionUpdateManager->getFieldStorageDefinition('rave_identifier', $entity_type_id));
  }

  private function getBundleSelectElement($id, $entity_type_id, $bundle_entity_type_id, $label, $default_value) {
    $element = [
      '#title' => $this->t('@label bundle', ['@label' => $label]),
      '#states' => [
        'visible' => $this->getCheckedInputStatesValue($id, $entity_type_id),
        'required' => $this->getCheckedInputStatesValue($id, $entity_type_id),
      ],
      '#default_value' => $default_value,
    ];
    if ($this->getBundleCount($bundle_entity_type_id) <= 20) {
      $element['#type'] = 'select';
      $element['#options'] = $this->getBundleOptions($bundle_entity_type_id);
    }
    else {
      $element['#type'] = 'entity_autocomplete';
      $element['#target_type'] = $bundle_entity_type_id;
    }
    return $element;
  }

  private function getCheckedInputStatesValue($id, $entity_type_id) {
    return [
      ':input[name="mapping['. $id . '][sync_' . $entity_type_id . ']"]' => ['checked' => TRUE],
    ];
  }

  private function getBundleCount($bundle_entity_type_id) {
    return $this->entityTypeManager->getStorage($bundle_entity_type_id)->getQuery()->count()->execute();
  }

  private function getBundleOptions($bundle_entity_type_id) {
    $bundles = $this->entityTypeManager->getStorage($bundle_entity_type_id)->loadByProperties();
    return array_map(function(EntityInterface $entity_type) {
      return $entity_type->label();
    }, $bundles);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('mapping') as $id => $mapping) {
      if (!empty($mapping['sync_node_mapping'])) {
        try {
          Yaml::decode($mapping['sync_node_mapping']);
        }
        catch (InvalidDataTypeException $e) {
          $form_state->setErrorByName('mapping][' . $id . '][sync_node_mapping', $this->t('Invalid yaml formatting'));
        }
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mapping = $form_state->getValue('mapping');
    $values = array_reduce($mapping, function($carry, $item) {
      if (empty($item['id'])) {
        return $carry;
      }
      $id = $item['id'];
      if (!empty($item['sync_node_mapping'])) {
        $item['sync_node_mapping'] = Yaml::decode(trim($item['sync_node_mapping']));
      }
      $carry[$id] = $item;
      return $carry;
    }, []);

    $config = $this->config('rave_alerts.settings');
    $config->set('mapping', $values);
    $config->save();
  }
}