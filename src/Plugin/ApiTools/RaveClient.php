<?php

namespace Drupal\rave_alerts\Plugin\ApiTools;

use Drupal\apitools\Annotation\ApiToolsClient;
use Drupal\apitools\Api\Client\ClientBase;
use Drupal\apitools\ClientManagerInterface;
use Drupal\apitools\ClientResourceManagerInterface;
use Drupal\Component\Datetime\Time;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\KeyRepositoryInterface;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines an ApiTools implementation for RaveClient Class.
 *
 * @ApiToolsClient(
 *   id = "rave",
 *   admin_label = @Translation("Rave Alert"),
 *   config = {
 *     "login" = {
 *        "title" = @Translation("Username/Password"),
 *        "type" = "key_select"
 *     },
 *     "url_listener" = {
 *        "title" = @Translation("Listener URL"),
 *        "description" = @Translation("The url for POST requests to Rave")
 *     }
 *   }
 * )
 */
class RaveClient extends ClientBase {

  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientManagerInterface $client_manager, ClientResourceManagerInterface $resource_manager, ConfigFactoryInterface $config_factory, KeyRepositoryInterface $key_repository, Time $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $client_manager, $resource_manager, $config_factory, $key_repository, $time);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setSerializer($container->get('serializer'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function auth() {
    $login = $this->getConfigValue('login');
    $this->options->set('headers', [
      'Authorization' => 'Basic ' . base64_encode($login['username'] . ':' . $login['password']),
      'Content-Type' => 'text/xml; charset=UTF8',
    ]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function postRequest($response) {
    if ($response) {
      return $this->serializer->decode($response, 'xml');
    }
    return parent::postRequest($response);
  }

  /**
   * Fetch alert from CAP outbound endpoint.
   *
   * @param $options
   *   Http client request options.
   *
   * @return array|mixed|null
   */
  public function alerts($options = []) {
    $outbound_url = $this->getConfigValue('url_outbound');
    $response = $this->auth()->request('get', $outbound_url, $options);
    return isset($response['identifier']) ? [$response] : $response;
  }

  public function sync(array $options = []) {
    $results = [];

    $mapping_definitions = $this->configFactory->get('rave_alerts.settings')->get('mapping');
    foreach ($mapping_definitions as $id => $mapping) {
      $outbound_url = $mapping['outbound_url'];
      $response = $this->auth()->request('get', $outbound_url, $options);
      $results[$id] = isset($response['identifier']) ? [$response] : $response;
    }

    return $results;
  }

  public function sendAlert(array $alert_data, array $options = []) {
    $request_date = DrupalDateTime::createFromTimestamp($this->time->getRequestTime());
    $format = 'Y-m-d\TH:i:sP';

    $default_alert_data = [
      '@xmlns' => 'urn:oasis:names:tc:emergency:cap:1.2',
      'identifier' => '',
      'sender' => '',
      'sent' => $request_date->format($format),
      'status' => '',
      'msgType' => '',
      'scope' => '',
      'info' => [],
    ];

    $alert_data = NestedArray::mergeDeep($default_alert_data, $alert_data);
    $xml = $this->serializer->encode($alert_data, 'xml', [XmlEncoder::ROOT_NODE_NAME => 'alert']);

    $listener_url = $this->getConfigValue('url_listener');
    $result = $this->post($listener_url, ['body' => $xml]);

    return $result === "" ? TRUE : FALSE;
  }
}
